﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleportion : MonoBehaviour
{
    public GameObject controller;
    public GameObject[] teleportPoints;
    public int teleportPointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.FindGameObjectWithTag("Controller");
        teleportPoints = GameObject.FindGameObjectsWithTag("TeleportPoint");
    }

    // Update is called once per frame
    void Update()
    {
        TeleportToPoint();
    }
    

    public void TeleportToPoint()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (teleportPointIndex < teleportPoints.Length)
            {
                controller.transform.position = teleportPoints[teleportPointIndex].transform.position;
                teleportPointIndex++;
            }
            else
            {
                teleportPointIndex = 0;
            }
        }
        
    }

}
