﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp_FirstTimeAttack_Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }


    public bool IsFirstTimeAttack;
    public float firstShootingCountDown;
    public float float_FirstShootDelayTime;
    public Vector3 e;
    public GameObject h;
    public Transform t;
    public float nextAttack;
    public float attackRate;
    // Update is called once per frame
    void Update()
    {
        TimeT();
        NextAttackTime();
        //vTest();
        // print("TestT");
    }

    void TimeT()
    {

        //Debug.Log("Do Attack State");
        if (IsFirstTimeAttack)//先檢視是不是第一次攻擊
        {
            firstShootingCountDown = float_FirstShootDelayTime;//給個時間
            IsFirstTimeAttack = false;//把第一次攻擊屬性調成false 去執行下面 else
        }
        else//不是第一次攻擊
        {
            firstShootingCountDown -= Time.deltaTime;//第一次攻擊時間倒數計時
            //Debug.Log("First Attack Preparing" + firstShootingCountDown);



            if (firstShootingCountDown < 0)//第一次攻擊時間倒數計時完後
            {
                //firstShootingCountDown = float_FirstShootDelayTime; //不斷重新計時
               
                return;
            }
        }
    }
    void NextAttackTime()
    {
        nextAttack += Time.deltaTime;
        if(nextAttack > attackRate)
        {

            //print("測試");

            nextAttack = 0;
        }
    }
    void vTest()
    {

        Vector3 t = h.transform.position;
        //Transform w = t; //Vector 無法直接轉換成 Transform 
        //print(t.x);

        

        foreach( Transform F in transform)
        {
            F.position += Vector3.up * 10;
        }
    }
    
    public class onTriggerTest : MonoBehaviour
    {

        public void OnTriggerEnter(Collider other)
        {
            print("Test"); /// 無法觸發 , 似乎同命名空間底下的第一個Class 才能觸發OnTriggerEnter
        }

    }
}

