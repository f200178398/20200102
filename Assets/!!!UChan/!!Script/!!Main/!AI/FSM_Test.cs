﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using SA;

/// <summary>
/// 要call的class，放在怪物身上
/// </summary>
public class FSM_Test : MonoBehaviour
{

    private StateMachine _stateMachine;
    public GameObject Controller;
    public float distanceToPlayer;

    public float searchingRange=8.0f;
    public float attackRange=2.0f;
    public AI_data data;
    // Start is called before the first frame update
    void Start()
    {
        //初始化，並給初始狀態
        data = GetComponent<AI_data>();
        _stateMachine = new StateMachine(StateType.Idle , data);
        Controller = GameObject.Find("Controller");
        //登錄狀態變化的線
        _stateMachine.AddTransition(StateType.Idle, StateType.Chase, TransitionType.GoTo_Chase);
        _stateMachine.AddTransition(StateType.Idle, StateType.Attack, TransitionType.GoTo_Attack);
        _stateMachine.AddTransition(StateType.Chase, StateType.Attack, TransitionType.GoTo_Attack);
        _stateMachine.AddTransition(StateType.Chase, StateType.Idle, TransitionType.GoTo_Idle);
        _stateMachine.AddTransition(StateType.Attack, StateType.Chase, TransitionType.GoTo_Chase);
        _stateMachine.AddTransition(StateType.Attack, StateType.Idle, TransitionType.GoTo_Idle);


    }



    // Update is called once per frame
    void Update()
    {
        distanceToPlayer = Vector3.Distance(this.transform.position, Controller.transform.position);

        //Debug.Log("Distance = " + distanceToPlayer);
        if (distanceToPlayer < searchingRange)
        {
            _stateMachine.ExecuteTransition(TransitionType.GoTo_Chase , data);
            //StartCoroutine(StopAction());
        }
        if (distanceToPlayer <attackRange)
        {
            _stateMachine.ExecuteTransition(TransitionType.GoTo_Attack , data);
           // StartCoroutine(StopAction());
        }
        

        //狀態機做事！
        _stateMachine.DoState(data);
    }


    //IEnumerator StopAction()
    //{
    //    Debug.Log("Stop Action now");
    //    yield return new WaitForSeconds(5);
    //}

    void OnDrawGizmos()
    {
        if (_stateMachine ==null)
        {
            return;
        }
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(this.transform.position, this.transform.position+this.transform.forward*5.0f);

        if (_stateMachine.GetCurrentState== StateType.Idle)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(this.transform.position, searchingRange);
        }
        else if (_stateMachine.GetCurrentState == StateType.Chase)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(this.transform.position, searchingRange);
        }
        else if (_stateMachine.GetCurrentState == StateType.Attack)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(this.transform.position, attackRange);
        }
      
    }

}

#region 把State和Transition拿來用的class
public class StateMachine
{
    private StateType _stateType;
    private State _state;

    //把狀態的名稱和狀態class存在一起(配對完成)
    private Dictionary<StateType, State> _stateTouroku = new Dictionary<StateType, State>();
    //把狀態之間變換的條件給串好
    private Dictionary<StateType, List<Transition>> _transitionList = new Dictionary<StateType, List<Transition>>();

    //變更狀態，要離開這個狀態了
    public void ChangeState(StateType stateType , AI_data data)
    {
        if (_state != null)//防呆，不是空的狀態才可以離開
        {
            _state.ExitState();
        }

        _stateType = stateType;//把()裡的狀態丟給現狀態，成為新的狀態，這邊只改enum而已
        _state = _stateTouroku[_stateType];//這裡利用Dictionary串連enum的真state，丟給現狀態，變成新狀態
        _state.EnterState(data);//(因為狀態已經改變了)做狀態的Enter

    }

    //在狀態時一直做什麼
    public void DoState(AI_data data)
    {
        _state.OnState(data);
    }

    //取得現在的狀態
    public StateType GetCurrentState
    {
        get { return _stateType; }
    }

    //初始化
    public StateMachine(StateType initialState , AI_data data)
    {
        _stateTouroku.Add(StateType.Idle, new State_Idle());
        _stateTouroku.Add(StateType.Chase, new State_Chase());
        _stateTouroku.Add(StateType.Attack, new State_Attack());

        ChangeState(initialState , data);//一開始就做什麼狀態
    }


    //增加變化條件(串起來)用的，但是裡面有很炫的LambDa語法我也不是很懂還要再研究
    public void AddTransition(StateType from, StateType to, TransitionType transitionType)
    {
        //如果串Goto的Dictionary裡沒有 from(原狀態)的話
        if(!_transitionList.ContainsKey(from))
        {
            _transitionList.Add(from, new List<Transition>());//就給他加一個(把他加到Dictionary裡)
        }
        List<Transition> addTransitions = _transitionList[from]; //把資料加到List裡(上面只有加到Dictionary而已)
        Transition tempTransition = addTransitions.FirstOrDefault(x => x.goToThisState == to);//抄來的很炫的Lambda
        if (tempTransition ==null)//如果上面那個是空的，就進行新登錄
        {
            addTransitions.Add(new Transition { goToThisState=to, transitionType=transitionType });//把要變的條件加進去
        }
        else//如果已經有的話，就給他串成新的
        {
            tempTransition.goToThisState = to;//要去哪一個狀態
            tempTransition.transitionType = transitionType;//條件
        }
    }

    //******這個還要再研究
    public void ExecuteTransition(TransitionType transitionType , AI_data data)//進行變化狀態的動作
    {
        List<Transition> soManyTransitions = _transitionList[_stateType];//加到Dictionary的List把他先暫存起來(取出來的意思)
        foreach (Transition everyTransition in soManyTransitions)//List裡面每個東西都要做
        {
            if (everyTransition.transitionType == transitionType)//如果要去下個狀態 和 丟進來的一樣
            {
                ChangeState(everyTransition.goToThisState,  data);//做變化狀態
                break;
            }
        }
    }


}
#endregion




#region 各種State(包含母體)

public enum StateType
{
    Idle,
    Chase,
    Attack
}

public abstract class State
{
    
    public virtual void EnterState(AI_data data)
    {
        Debug.Log("Enter Null State");
    }
    public virtual void OnState(AI_data data)
    {
        Debug.Log("In Null State");
    }
    public virtual void ExitState()
    {
        Debug.Log("Exit Null State");
    }
}

public class State_Idle : State
{
    public override void EnterState(AI_data data)
    {
        Debug.Log("Enter Idle State");
    }
    public override void OnState(AI_data data)
    {
       AI_Function.Idle(data);
        Debug.Log("In Idle State");
    }
    public override void ExitState()
    {
        Debug.Log("Exit Idle State");
    }
}

public class State_Chase : State
{
    public override void EnterState(AI_data data)
    {
        Debug.Log("Enter Chase State");
    }
    public override void OnState(AI_data data)
    {
        Debug.Log("In Chase State");
    }
    public override void ExitState()
    {
        Debug.Log("Exit Chase State");
    }
}

public class State_Attack : State
{
    public override void EnterState(AI_data data)
    {
        Debug.Log("Enter Attack State");
    }
    public override void OnState(AI_data data)
    {
       
        Debug.Log("In Attack State");
    }
    public override void ExitState()
    {
        Debug.Log("Exit Attack State");
    }
}
#endregion

/// <summary>
/// 各種GoTo什麼狀態的標籤
/// </summary>

#region 串連用的條件
public class Transition
{
    public StateType goToThisState { get; set; }
    public TransitionType transitionType { get; set; }
}

public enum TransitionType
{
    GoTo_Idle,
    GoTo_Chase,
    GoTo_Attack
}
#endregion

