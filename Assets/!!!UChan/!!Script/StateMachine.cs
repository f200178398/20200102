﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DA
{
    public class StateMachine : MonoBehaviour
    {
        public float vertical;
        public float horizontal;
         Animator anim;
        public GameObject activeModel;
        public float moveAmount;
        public float moveSpeed = 2;
        public float runSpeed = 3.5f;
        public float rotateSpeed = 50;

        Vector3 moveDir = Vector3.zero;
        float delta;
        public Rigidbody rigi;

        public void Init()
        {
            rigi = GetComponent<Rigidbody>();
        }

        public void StepAnimator()
        {
            //anim.applyRootMotion = false;
            anim = GetComponent<Animator>();
            if (anim == null)
            {
                Debug.Log("no model found");
            }
            else
            {
                activeModel = anim.gameObject;
            }
        }

        public void Movement()
        {
            delta = Time.deltaTime;
            vertical = Input.GetAxis("Vertical") * moveSpeed;
            horizontal = Input.GetAxis("Horizontal") * rotateSpeed;
            moveDir = new Vector3(0, 0, vertical);
            rigi.velocity = moveDir * moveSpeed;
            float targetSpeed = moveSpeed;
            Vector3 targetDir = moveDir * (targetSpeed * moveAmount);
            targetDir.y = 0;
            if (targetDir == Vector3.zero)
                targetDir = transform.forward;
            Quaternion tr = Quaternion.LookRotation(targetDir);
            Quaternion targetRotation = Quaternion.Slerp(transform.rotation, tr, delta * moveAmount * rotateSpeed);
            transform.rotation = targetRotation;
            //rigi.rotation = Quaternion.Euler(0,horizontal*rigi.velocity.x * delta, 0 );
            //transform.Translate(0, 0, vertical * delta);
            //transform.Rotate(0,horizontal * delta, 0);
            //if (Input.GetKeyDown(KeyCode.W))
            //{
            //    Debug.Log("work");
            //}
        }
    }
}

