﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastBossJumpEffectSound : MonoBehaviour
{
    public LastBossJumpEffectSoundPool lastBossJumpEffectSoundPool;
    [Header("Sound Settings")]
    public AudioSource as_jumpSound;
    [Header("Time Settings")]
    public float _timer=0;
    public float lifeTime = 3f;

    // Start is called before the first frame update
    void Awake()
    {
        lastBossJumpEffectSoundPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LastBossJumpEffectSoundPool>();
        as_jumpSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
        as_jumpSound.Play();
    }

    void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + lifeTime)
        {
            lastBossJumpEffectSoundPool.Recovery(gameObject);
        }
    }
}
