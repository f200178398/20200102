﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBeHitSoundEffect : MonoBehaviour
{
    public PlayerBeHitSoundeffectPool playerBeHitSoundeffectPool;
    [Header("Sound Settings")]
    public AudioSource as_PlayerBeHitSound;
    public AudioClip[] ac_PlayerBeHitSoundArray=null;
    public int i_indexPlayerBeHitSound=0;
    [Header("Time Settings")]
    public float _timer;
    public float lifeTime = 3f;

    // Start is called before the first frame update
    void Awake()
    {
        playerBeHitSoundeffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerBeHitSoundeffectPool>();
        i_indexPlayerBeHitSound = Random.Range(0, ac_PlayerBeHitSoundArray.Length);
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
        as_PlayerBeHitSound.clip = ac_PlayerBeHitSoundArray[i_indexPlayerBeHitSound];
        as_PlayerBeHitSound.Play();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer+lifeTime)
        {
            playerBeHitSoundeffectPool.Recovery(gameObject);
        }
    }
}
