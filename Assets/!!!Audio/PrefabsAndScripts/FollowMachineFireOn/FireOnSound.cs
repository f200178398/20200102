﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireOnSound : MonoBehaviour
{
    public AudioSource as_FireOnEffectSound;

    // Start is called before the first frame update
    void Awake()
    {
        as_FireOnEffectSound = GetComponent<AudioSource>();
    }

    

    private void OnEnable()
    {
        as_FireOnEffectSound.Play();
    }

}
